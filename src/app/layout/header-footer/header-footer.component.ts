import { Component } from '@angular/core';
import { HeaderComponent } from '../header/header.component';
import { FooterComponent } from '../footer/footer.component';


//Shared Module
import { SharedModule } from '../../shared/shared.module';

@Component({
  selector: 'app-header-footer',
  standalone: true,
  imports: [HeaderComponent,FooterComponent,SharedModule],
  templateUrl: './header-footer.component.html',
  styleUrl: './header-footer.component.scss'
})
export class HeaderFooterComponent {

}
