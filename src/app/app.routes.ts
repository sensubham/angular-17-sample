import { Routes } from '@angular/router';

import { HeaderFooterComponent } from './layout/header-footer/header-footer.component';


export const routes: Routes = [
    {
        path: '',
        pathMatch: 'full',
        redirectTo: 'main',
      },
      {
        path: 'main',
        component: HeaderFooterComponent,
        loadChildren: () => import('./main/main.module').then((m) => m.MainModule),
      },
];
