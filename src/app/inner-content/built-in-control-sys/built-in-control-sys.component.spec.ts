import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BuiltInControlSysComponent } from './built-in-control-sys.component';

describe('BuiltInControlSysComponent', () => {
  let component: BuiltInControlSysComponent;
  let fixture: ComponentFixture<BuiltInControlSysComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [BuiltInControlSysComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(BuiltInControlSysComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
