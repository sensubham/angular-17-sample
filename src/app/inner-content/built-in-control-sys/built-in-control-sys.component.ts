import { Component, OnInit } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';

@Component({
  selector: 'app-built-in-control-sys',
  standalone: true,
  imports: [SharedModule],
  templateUrl: './built-in-control-sys.component.html',
  styleUrl: './built-in-control-sys.component.scss'
})
export class BuiltInControlSysComponent implements OnInit{
  isAngularDeveloper!:boolean;

  constructor(){}
  ngOnInit():void{}

  yesButtonClick():void{
    this.isAngularDeveloper = true
  }
  noButtonClick():void{
    this.isAngularDeveloper = false;
  }
}
