import { Component, OnInit, ViewChild, computed, signal } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { MatTable } from '@angular/material/table';

export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  { position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H' },
  { position: 2, name: 'Helium', weight: 4.0026, symbol: 'He' },
  { position: 3, name: 'Lithium', weight: 6.941, symbol: 'Li' },
/*   { position: 4, name: 'Beryllium', weight: 9.0122, symbol: 'Be' },
  { position: 5, name: 'Boron', weight: 10.811, symbol: 'B' },
  { position: 6, name: 'Carbon', weight: 12.0107, symbol: 'C' },
  { position: 7, name: 'Nitrogen', weight: 14.0067, symbol: 'N' },
  { position: 8, name: 'Oxygen', weight: 15.9994, symbol: 'O' },
  { position: 9, name: 'Fluorine', weight: 18.9984, symbol: 'F' },
  { position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne' }, */
];

@Component({
  selector: 'app-signal',
  standalone: true,
  imports: [SharedModule],
  templateUrl: './signal.component.html',
  styleUrl: './signal.component.scss',
})
export class SignalComponent implements OnInit {
  @ViewChild(MatTable, { static: true })
  table!: MatTable<any>;

  displayedColumns: string[] = ['position', 'name', 'weight', 'symbol'];
  dataSource = ELEMENT_DATA;

  firstName = signal('Subham');
  lastName = signal('Sen');

  fullName = computed(() => this.firstName() + ' ' + this.lastName()); //when change the dependencies of full name ,full name also change

  //Quantity
  qty = signal<number>(0);
  constructor() {}

  ngOnInit(): void {}

  changeFirstName(event: string): void {
    this.firstName.set(event);
  }

  changeSecondName(event: string): void {
    this.lastName.set(event);
  }
  increaseQuantity(): void {
    this.qty.update((q) => q + 1);
  }
  decreaseQuantity(): void {
    this.qty.update((q) => q - 1);
  }
}
