import { Component, OnInit } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { AppComponent } from '../../app.component';
import { BuiltInControlSysComponent } from '../../inner-content/built-in-control-sys/built-in-control-sys.component';
import { SignalComponent } from '../../inner-content/signal/signal.component';


@Component({
  selector: 'app-new-features',
  standalone: true,
  imports: [SharedModule,AppComponent,BuiltInControlSysComponent,SignalComponent],
  templateUrl: './new-features.component.html',
  styleUrl: './new-features.component.scss',
})
export class NewFeaturesComponent {
  
  constructor(private appData : AppComponent) {
    console.log('DATA -------------------->>>>',this.appData.title)
  }
}
