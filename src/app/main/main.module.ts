import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MainRoutingModule } from './main-routing.module';
import { SharedModule } from '../shared/shared.module';
import { CdkColumnDef } from '@angular/cdk/table';

@NgModule({
  declarations: [
 
  ],
  providers: [CdkColumnDef],
  imports: [
    CommonModule,
    MainRoutingModule,
    SharedModule,
  ]
})
export class MainModule { }
