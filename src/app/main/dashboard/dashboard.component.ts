import { Component } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { Router } from '@angular/router'


@Component({
  selector: 'app-dashboard',
  standalone: true,
  imports: [SharedModule],
  templateUrl: './dashboard.component.html',
  styleUrl: './dashboard.component.scss'
})
export class DashboardComponent {
  dashboardVar !: string ;
  constructor(
    private router:Router,
    
  ){}

  changeRoute():void{
    this.router.navigateByUrl('/main/new-features');
    this.dashboardVar = 'Sakabda das'
    console.log('***********************************************************************')
  }
}
