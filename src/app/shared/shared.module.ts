import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

//ANGULAR MATERIALS
import {MatCardModule} from '@angular/material/card';
import {MatButtonModule} from '@angular/material/button';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatFormFieldModule} from '@angular/material/form-field';
import {FormsModule} from '@angular/forms';
import {MatInputModule} from '@angular/material/input';
import {MatTableModule} from '@angular/material/table';


//RouterOutlet
import { RouterModule, RouterOutlet } from '@angular/router';
// import { Router } from '@angular/router'


const imports = [
  MatCardModule,
  MatButtonModule,
  RouterOutlet,
  MatGridListModule,
  RouterModule,
  MatFormFieldModule,
  FormsModule,MatInputModule,
  MatTableModule
]
@NgModule({
  declarations: [],
  imports: [
    CommonModule,...imports
  ],
  exports:[...imports]
})
export class SharedModule { }
